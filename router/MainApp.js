import React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome5';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Home from '../pages/Home/Home';
import Trending from '../pages/Trending/Trending';
import Kategori from '../pages/Kategori/Kategori';
import Bookmark from '../pages/Bookmark/Bookmark';


const Tab = createMaterialBottomTabNavigator();

const MainApp = () =>{
    return(
        <Tab.Navigator  barStyle = {{backgroundColor :'#5bb7e5',paddingHorizontal: 50}} inactiveColor = "#fff" activeColor = '#fff' labeled = {true} shifting = {false} >
            <Tab.Screen name="Home" component={Home} 
            options={{
                tabBarLabel: 'Home',
                
                tabBarIcon: ({ color }) => (
                    <Icon2 name="home" size={26} color= {color} />
            ),
                
            }}  />
            <Tab.Screen name="Trending" component={Trending}
            options={{
                tabBarLabel: 'Trending',
                tabBarIcon: ({ color }) => (
                    <Icon name="fire" size={26} color= {color} />
            ),
            }}  />
            <Tab.Screen name="Kategori" component={Kategori} 
            options={{
                tabBarLabel: 'Kategori',
                tabBarIcon: ({ color }) => (
                    <Icon name="list" size={26} color= {color} />
            ),
            }}  />
            <Tab.Screen name="Bookmark" component={Bookmark} 
            options={{
                tabBarLabel: 'Bookmark',
                tabBarIcon: ({ color }) => (
                    <Icon2 name="bookmark" size={26} color= {color} />
            ),
            }}  />
      </Tab.Navigator>
    )
}

export default MainApp