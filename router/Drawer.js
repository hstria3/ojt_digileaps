import * as React from 'react';
import { Button, View } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import Sidebar from '../component/Customdrawer'
import Icon from 'react-native-vector-icons/FontAwesome'
import MainApp from './MainApp'

const Drawer = createDrawerNavigator();

const DrawNav= () => {
  return (
    <Drawer.Navigator drawerPosition = "right"  initialRouteName="Profile"  drawerStyle = {{backgroundColor : '#eef0f9'}}
      drawerContent = {props => <Sidebar {...props} /> }>
        <Drawer.Screen name="MainApp" component={MainApp}/>
        
    </Drawer.Navigator>
  );
}

export default DrawNav;