import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import Splash from '../pages/Splash/splash'
import Slider from "../pages/Slider/slider";
import MainApp from "./MainApp";
import DrawNav from "./Drawer";
import Login from "../pages/Login/Login"
import Ai from "../pages/AI/AI"
import NewsList from "../pages/NewsList/NewsList"
import News from "../pages/News/News"
import NewsVideo from "../pages/NewsVideo/NewsVideo"
import Detail from "../pages/NewsDetail/Detail"
import DetailNewsVideo from "../pages/NewsVideo/index"


const Stack = createStackNavigator();


const Router = () => {
    return (
        <Stack.Navigator initialRouteName="Splash">
            <Stack.Screen name = "Splash" component ={Splash} options ={{headerShown : false}}/>
            <Stack.Screen name = "Slider" component ={Slider} options ={{headerShown : false}}/>
            
            <Stack.Screen name = "MainApp" component ={MainApp} options ={{headerShown : false}}/>
            <Stack.Screen name = "Login" component ={Login} options ={{headerShown : false}}/>
            <Stack.Screen name = "DrawNav" component = {DrawNav} options ={{
                headerShown :false}}
            />
            <Stack.Screen name = "NewsList" component ={NewsList} options ={{headerShown : false}}/>
            <Stack.Screen name = "News" component ={News} options ={{headerShown : false}}/>
            <Stack.Screen name = "NewsVideo" component ={NewsVideo} options ={{headerShown : false}}/>
            <Stack.Screen name = "Detail" component ={Detail} options ={{headerShown : false}}/>

            <Stack.Screen name = "Ai" component ={Ai} options ={{headerShown : false}}/>
            <Stack.Screen name = "DetailNewsVideo" component ={DetailNewsVideo} options ={{headerShown : false}}/>
        </Stack.Navigator>
    )
}

export default Router

