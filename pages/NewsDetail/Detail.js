import React,{Component} from 'react';
import {Text,View,Button,StyleSheet,ScrollView} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Header from '../../component/Header';
import NewsList from '../NewsList/NewsList';
import News from '../News/News';
const Detail = ({navigation}) => {
    return ( 
        <View>
            <Header title = {"Berita"} nav = {navigation}/>
            <View style = {{padding: 15, flexDirection :"row",borderBottomWidth : 1, borderBottomColor : "gray"}}>
                <View>
                    <Text style = {styles.menuBar} >Terbaru</Text>
                </View>
                
                <Text style = {styles.menuBar}>Berita Utama</Text>
                <Text style = {styles.menuBar}>Most Populer</Text>
                <Text style = {styles.menuBar}>Most Populer</Text>
            </View>
            
            <News />
        </View>
    );
    
}

const styles = StyleSheet.create({
    menuBar :{
        marginRight : 10,
        fontSize : 16,
        color :"#737373",
        
    }
})
 
 
export default Detail;