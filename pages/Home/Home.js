import React,{Component} from 'react';
import {Text,View,Button,Dimensions,StyleSheet,ScrollView} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HeaderHome from '../../component/HeaderHome'
import Terbaru from '../Terbaru/Terbaru';
import NewsList from '../NewsList/NewsList'

const Home = ({navigation}) =>{
    return(
        <View>
            <HeaderHome title = {"Digileaps"} nav = {navigation}/>
            <View style = {{padding: 15, flexDirection :"row",borderBottomWidth : 1, borderBottomColor : "gray"}}>
                <View>
                    <Text style = {styles.menuBar}>Terbaru</Text>
                </View>
                
                <Text style = {styles.menuBar} onPress = {()=> navigation.navigate("DetailNewsVideo")}>Berita Utama</Text>
                <Text style = {styles.menuBar}>Most Populer</Text>
                <Text style = {styles.menuBar}>Most Populer</Text>
            </View>
            <ScrollView>
                <NewsList nav = {navigation}/>
            </ScrollView>
            
            
           
            
            {/* <Button title = 'Menuju Trending' onPress={() => navigation.navigate('Trending') }/>  */}
        </View>
         
    )
}


const styles = StyleSheet.create({
    menuBar :{
        marginRight : 10,
        fontSize : 16,
        color :"#737373",
        
    }
})
 
export default Home;

