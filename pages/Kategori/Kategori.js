import React from 'react'
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import Header from '../../component/Header';

const Kategori = ({navigation}) => {
    return (
        <View>
            <Header title = {"Kategori"} nav = {navigation}/>
            <View style={styles.container}>
            
                <TouchableOpacity style={styles.appButtonContainer} onPress = {()=>{
                    navigation.navigate("Ai");
                }}>
                    <Text style={styles.appButtonText}>Artificial Intelligence</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.appButtonContainer}>
                    <Text style={styles.appButtonText}>Business</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.appButtonContainer}>
                    <Text style={styles.appButtonText}>Internet of Things</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.appButtonContainer}>
                    <Text style={styles.appButtonText}>Information Technology</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.appButtonContainer}>
                    <Text style={styles.appButtonText}>Start Up</Text>
                </TouchableOpacity>
            </View>
        </View>
        
    )
}

export default Kategori

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 50,
        backgroundColor: "#eef0f9",
      },
    
      appButtonContainer: {
        elevation: 8,
        backgroundColor: "white",
        borderRadius: 10,
        paddingVertical: 25,
        paddingHorizontal: 12,
        marginTop: 12,
        justifyContent : "center"
        
      },
      appButtonText: {
        fontSize: 14,
        color: "black",
        alignSelf: "baseline",
        paddingLeft: 50,
      },
})
