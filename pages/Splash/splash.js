import React, { useEffect } from 'react';
import {StyleSheet, Text, View, Image} from 'react-native'
import {logo} from '../../assets';

const splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() =>{
            navigation.replace("Slider")
        },3000)
    }, [navigation])
    
    return (
        <View style={styles.container}>
            <Image style = {styles.logo} resizeMode={'stretch'} source={logo}/>
            <Text style = {styles.version}>Versi 2.0.0.1</Text>
            
        </View>
    )
}

export default splash


const styles = StyleSheet.create({
    container:{
        backgroundColor: '#eef0f9',
        alignItems: 'center',
        flex : 1,

        justifyContent: 'center',
    },

    logo:{
        width :280,
        height : 250,
        marginTop : 180
    },
    version:{
      color :"gray",
      paddingTop : 160
    }
})