import React,{Component} from 'react';
import { View, Text, ScrollView, Dimensions,Image,SafeAreaView,StyleSheet,TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import Home from "../Home/Home"
import Swiper from 'react-native-swiper'


export default class Slider extends Component{
  
  state = {
    isModalVisible: false,
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };



  renderModal() {
    return (
      <View style={styles.viewModal}>
        <Text style = {styles.textTitle}>Syarat dan Ketentuan</Text>
        <ScrollView>
        <Text style={styles.textModal}> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vestibulum auctor turpis. In hac habitasse platea dictumst. Morbi dapibus vestibulum sapien, ac iaculis lacus fringilla vitae. Sed lacus risus, tempus sed tincidunt fringilla, rhoncus vel massa. Curabitur egestas suscipit mi id tincidunt. Integer sit amet pretium magna. Duis auctor, orci eget imperdiet imperdiet, felis elit luctus enim, sit amet vestibulum justo enim egestas ipsum. Sed dignissim eget velit nec venenatis.
        </Text>
        <Text style={styles.textModal}> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vestibulum auctor turpis. In hac habitasse platea dictumst. Morbi dapibus vestibulum sapien, ac iaculis lacus fringilla vitae. Sed lacus risus, tempus sed tincidunt fringilla, rhoncus vel massa. Curabitur egestas suscipit mi id tincidunt. Integer sit amet pretium magna. Duis auctor, orci eget imperdiet imperdiet, felis elit luctus enim, sit amet vestibulum justo enim egestas ipsum. Sed dignissim eget velit nec venenatis.
        </Text>

        </ScrollView>
        <TouchableOpacity style = {styles.button}  onPress={()=>{
          this.setState({isModalVisible : !this.state.isModalVisible})
          this.props.navigation.replace("Login")
        }} color="#41A8CC">
          <Text style = {styles.textBtn}>Saya Setuju</Text>
        </TouchableOpacity>
      </View>
    );
  }
  render(){
    return(
      <Swiper loop = {false} dotStyle = {{width : 15}} activeDotStyle = {{width : 30}}>
          <View style = {styles.widthWindow}>
              <View style = {{alignItems : "center"}}>
                {/* <Image source = {require('./assets/pic01.jpg')} style ={{width:300,height:300,resizeMode:'cover'}} /> */}
                <Text style = {{
                    fontSize:30,
                    fontWeight:'bold',
                    color:'#5bb7e5',
                    
                    paddingTop:40

                    }} >Berita Terbaru</Text>
                  <Text style = {{
                    fontSize:14,
                    color:'gray',
                    marginTop : 20,
                    marginBottom : 80

                    }} >Ini Welcome yang pertama</Text>
                
              </View>
                    
            </View>
            <View style = {styles.widthWindow}>
              <View style = {{alignItems : "center"}}>
                {/* <Image source = {require('./assets/pic01.jpg')} style ={{width:300,height:300,resizeMode:'cover'}} /> */}
                <Text style = {{
                    fontSize:30,
                    fontWeight:'bold',
                    color:'#5bb7e5',
                    
                    paddingTop:40

                    }} >Berita Terkini</Text>
                  <Text style = {{
                    fontSize:14,
                    color:'gray',
                    marginTop : 20,
                    marginBottom : 80

                    }} >Ini Welcome yang kedua</Text>
                
              </View>
                    
            </View>
          <View style = {styles.widthWindow}>
              <View style = {{alignItems : "center"}}>

                <Text style = {{
                    fontSize:30,
                    fontWeight:'bold',
                    color:'#5bb7e5',
                    
                    paddingTop:40

                    }} >Berita Trending</Text>
                  <Text style = {{
                    fontSize:14,
                    color:'gray',
                    marginTop : 20,
                    marginBottom : 80

                    }} >Ini Welcome yang ketiga</Text>

                  <TouchableOpacity style = {styles.button}  onPress={this.toggleModal} >
                    <Text style = {styles.textBtn}>Mulai</Text>
                  </TouchableOpacity>
                  <Modal isVisible={this.state.isModalVisible}>
                  {this.renderModal()}
                  </Modal>
              </View>
                    
            </View>
      </Swiper>
      
      
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textTitle :{
    alignItems : 'center',
    fontSize : 18,
    fontWeight : 'bold'
  },
  viewModal: {
    width: 350,
    height: 550,
    backgroundColor: '#eef0f9',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    padding: 20
  },
  image: {
    width: 150,
    height: 150,
  },
  textModal: {
    marginVertical: 20,
  },
  button: {
    alignItems: "center",
    backgroundColor: "#5bb7e5",
    color : "white",
    width : 300,
    padding: 10,
    borderRadius : 10
  },
  textBtn :{
    color : "white",
    fontSize : 16,
    fontWeight : 'bold'
  },
  widthWindow:{
    width: Dimensions.get('window').width,
    // height: Dimensions.get('window').height
    backgroundColor : "#eef0f9",
    flex : 1,
    justifyContent : 'center',
    alignItems : 'center',
    
  }
  
});