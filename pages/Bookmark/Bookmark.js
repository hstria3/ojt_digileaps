import React from 'react'
import { StyleSheet, Text, View,ScrollView } from 'react-native'
import Header from '../../component/Header';
import NewsList from '../NewsList/NewsList'

const Bookmark = ({navigation}) => {
    return (
        <View>
            <Header title = {"Bookmark"} nav = {navigation}/>
            <ScrollView>
                <NewsList nav = {navigation}/>
            </ScrollView>
           
        </View>
    )
}

export default Bookmark

const styles = StyleSheet.create({})
