import React,{Component} from 'react';
import {Text,View,Button,Dimensions,StyleSheet,ScrollView} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Header from '../../component/Header'
import Terbaru from '../Terbaru/Terbaru';
import NewsList from '../NewsList/NewsList'

const AI = ({navigation}) =>{
    return(
        <View>
            <Header title = {"#artificial Intelligence"} nav = {navigation}/>
            
            <ScrollView>
                <NewsList nav = {navigation}/>
            </ScrollView>
           
            
            {/* <Button title = 'Menuju Trending' onPress={() => navigation.navigate('Trending') }/>  */}
        </View>
         
    )
}


const styles = StyleSheet.create({
    menuBar :{
        marginRight : 10,
        fontSize : 16,
        color :"#737373",
        
    }
})
 
export default AI;

