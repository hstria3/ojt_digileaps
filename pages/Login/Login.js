import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Text,
  TextInput,

} from 'react-native';
import {logo} from '../../assets';
import CheckBox from '@react-native-community/checkbox';

function Login({navigation}) {
  return (
    <View style={styles.container}>
      <Image
        style={styles.logo}
        source={logo}
        resizeMode={'stretch'}
      />
      <TextInput style={styles.inputText} placeholder="Email" />
      <TextInput style={styles.inputText} placeholder="Password" />
      <View style = {{flexDirection:"row",}}>
        <CheckBox  
          />
          <Text style={styles.label}>Keep me sign in</Text>
          <Text style = {{margin : 8, color:'blue',marginLeft : 120}}>Forgot Password?</Text>
      </View>
      <TouchableOpacity style={styles.appButtonContainer} onPress = {()=>{
        navigation.replace("DrawNav");
      }}>
        <Text style={styles.appButtonText}>Login</Text>
      </TouchableOpacity>
      <View style = {{flexDirection:"row",marginTop : 10}}>
        <Text>Don't have an account? </Text>
        <Text style = {{color :'blue' }}>Sign Up</Text>
      </View>
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#eef0f9',
    flex: 1,
    alignItems: 'center',
    justifyContent :"center",
  },
  logo: {
    height: 170,
    width: 200,
  },
  appButtonContainer: {
    backgroundColor: '#5bb7e5',
    borderRadius: 15,
    paddingHorizontal: 160,
    paddingVertical: 15,
    marginTop: 20,
  },
  appButtonText: {
    fontSize: 22,
    color: 'white',
    fontWeight: 'bold',
  },
  inputText: {
   
    height: 55,
    width: 375,
    marginBottom: 15,
    fontSize: 20,
    paddingHorizontal: 10,
    borderColor: 'lightgrey',
    borderStyle: 'solid',
    borderWidth: 2,
  },
  label :{
    fontSize : 14,
    margin : 6
    
  }
});
export default Login;
