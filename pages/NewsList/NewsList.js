import React, {useState} from 'react';
import {
  FlatList,
  Image,
  View,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5'

const DATA = [
  {
    id: 'id1',
    title: 'Judul Sebuah Berita 1',
  },
  {
    id: 'id2',
    title: 'Judul Sebuah Berita 2',
  },
  {
    id: 'id3',
    title: 'Judul Sebuah Berita 3',
  },
  {
    id: 'id4',
    title: 'Judul Sebuah Berita 4',
  },
  {
    id: 'id5',
    title: 'Judul Sebuah Berita 5',
  },
  {
    id: 'id6',
    title: 'Judul Sebuah Berita 6',
  },
];8

const Item = ({item, onPress, style}) => (
  <TouchableOpacity onPress={onPress} style={[styles.item, style]}>
    <Image
      style={styles.newspic}
      source={require("../../assets/images/tech.jpg")}
    />
    <View>
      <View style= {{flexDirection : "row"}}>
        <Text style={styles.title}>{item.title}</Text>
        <Icon name="crown" size={16} color= {"gold"} />
      </View>
      
      <View style = {{flexDirection : "row", padding : 10, marginTop : 20}}>
        <Text style = {{marginRight:28,color : "#3f80a0"}}>20 Min read | 20 Oct 2021</Text>
        <Icon name="bookmark" size={26} color= {"#5bb7e5"} />
      </View>
      
    </View>
    
    
    
  </TouchableOpacity>
);
const NewsList = ({nav}) => {
  const [selectedId, setSelectedId] = useState(null);

  const renderItem = ({item}) => {
    const backgroundColor = item.id === selectedId ? '#eef0f9' : '#eef0f9';

    return (
      <Item
        item={item}
        onPress={() => {
          setSelectedId(item.id)
          nav.navigate("Detail")
        }}
        style={{backgroundColor}}
      />
    );
  };

  return (
    <View style={styles.container}>
      <FlatList 
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        extraData={selectedId}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#eef0f9',
  },
  item: {
    padding: 1,
    marginVertical: 12,
    marginHorizontal: 16,
    flex: 2,
    flexDirection: 'row',
  },
  title: {
    fontSize: 18,
    marginLeft: 10,
    marginRight:10
  },
  newspic: {
    width: 140,
    height: 90,
    borderRadius: 20,
  },
});

export default NewsList;
