import React from 'react';
import {Image, View, StyleSheet, Text, ScrollView} from 'react-native';
import tech from '../../assets'
import Icon from 'react-native-vector-icons/FontAwesome5'
function News(props) {
  return (
    <ScrollView>
      <View style={styles.container}>

        <View style={styles.containerHeaderNews}>

          <Text style={styles.newsTitle}>Sebuah Judul Berita</Text>
          <View style ={{flexDirection : "row"}}>
            <View >
              
              <Text style={styles.newsAuthor}>Author: Aji</Text>
              <Text style={styles.newsDate}>Date</Text>
            </View>
            <View style = {{marginLeft : 240}}>
              <Icon name="bookmark" size={26} color= {"#5bb7e5"} />
            </View>
          </View>
            
            
        </View>
        
        <View>
          
        </View>
        <Image
          style={styles.newsImage}
          source={require("../../assets/images/tech.jpg")}
        />
        <Text style={styles.captionImage}>Foto</Text>

        <Text style={styles.news}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in the 1960s with
          the release of Letraset sheets containing Lorem Ipsum passages, and
          more recently with desktop publishing software like Aldus PageMaker
          including versions of Lorem Ipsum.
        </Text>
        <Text style={styles.news}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in the 1960s with
          the release of Letraset sheets containing Lorem Ipsum passages, and
          more recently with desktop publishing software like Aldus PageMaker
          including versions of Lorem Ipsum.
        </Text>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#eef0f9',
    flex: 1,
    alignItems: 'center',
  },
  //   scroll: {
  //     flexGrow: 1,
  //     marginHorizontal: 20,
  //     width: '100%',
  //     alignSelf: 'center',
  //     height: '20%',
  // },
  captionImage: {
    alignSelf: 'flex-start',
    paddingHorizontal: '5%',
  },
  newsImage: {width: '90%', height: 300},
  newsTitle: {
    fontSize: 22,
    alignSelf: 'center',
    paddingVertical: 16,
  },
  newsAuthor: {
    alignSelf: 'flex-start',
    color: 'blue',
  },
  newsDate: {
    alignSelf: 'flex-start',
  },
  containerHeaderNews: {
    width: '80%',
    marginBottom : 10
  },
  news: {
    width: '85%',
    fontSize: 18,
    lineHeight: 28,
    marginTop: 10,
  },
});
export default News;
