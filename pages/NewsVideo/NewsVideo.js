import React from 'react';
import {View, StyleSheet, Text,TextInput} from 'react-native';
import Video from 'react-native-video';
import Icon from 'react-native-vector-icons/FontAwesome5';


class NewsVideo extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerHeaderNews}>
          <Text style={styles.newsTitle}>
            Why Big Tech Is Getting Into Finance
          </Text>
          <View style ={{flexDirection : "row"}}>
            <View >
              
              <Text style={styles.newsAuthor}>Author: Aji</Text>
              <Text style={styles.newsDate}>Date</Text>
            </View>
            <View style = {{marginLeft : 240}}>
              <Icon name="bookmark" size={26} color= {"#5bb7e5"} />
            </View>
          </View>
        </View>
        <Video
          source={require('../../assets/video/videoplayback.mp4')} // Can be a URL or a local file.
          ref={(ref) => {
            this.player = ref;
          }} // Store reference
          onBuffer={this.onBuffer} // Callback when remote video is buffering
          onError={this.videoError} // Callback when video cannot be loaded
          pla
          resizeMode={'contain'}
          controls={true}
          style={styles.playVideo}
        />
        <Text style={styles.captionVideo}>Video By: Wall Street Journal</Text>

        <View style = {{justifyContent:"flex-end",alignItems:"flex-end",marginLeft:220}}>
          <Icon name="share" size={26} color= {"#5bb7e5"} />
          <Text>Bagikan</Text>
        </View>

        
        <View>
        <View >
          <Text style={{fontWeight:"bold",marginBottom:20,fontSize:18}}>Komentar (3)</Text>
          <TextInput
            style={{height:100,backgroundColor:"#fff",width:350,padding : 20}}
            placeholder="Berikan komentar anda"
            
          />
        </View>  
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  playVideo: {
    // position: 'absolute',
    // top: 0,
    // left: 0,
    // bottom: 0,
    // right: 0,
    width: '90%',
    height: 230,
    //alignSelf: 'center',
  },
  container: {
    backgroundColor: '#eef0f9',
    flex: 1,
    alignItems: 'center',
  },
  newsTitle: {
    fontSize: 22,
    alignSelf: 'center',
    paddingVertical: 16,
  },
  newsAuthor: {
    alignSelf: 'flex-start',
    color: 'blue',
  },
  newsDate: {
    alignSelf: 'flex-start',
  },
  containerHeaderNews: {
    width: '80%',
  },
  captionVideo: {
    alignSelf: 'flex-start',
    paddingHorizontal: '5%',
  },
});
export default NewsVideo;
