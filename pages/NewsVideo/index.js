import React from 'react'
import { StyleSheet, Text, View,ScrollView } from 'react-native'
import Header from '../../component/Header';
import NewsVideo from './NewsVideo';
const index = ({navigation}) => {
    return (
        <View>
            <Header title = {"Berita"} nav = {navigation}/>
            <View style = {{padding: 15, flexDirection :"row",borderBottomWidth : 1, borderBottomColor : "gray"}}>
                <View>
                    <Text style = {styles.menuBar} >Terbaru</Text>
                </View>
                
                <Text style = {styles.menuBar}>Berita Utama</Text>
                <Text style = {styles.menuBar}>Most Populer</Text>
                <Text style = {styles.menuBar}>Most Populer</Text>
            </View>
            
            <ScrollView>
                <NewsVideo />
            </ScrollView>
            
        </View>
    )
}

export default index

const styles = StyleSheet.create({
    menuBar :{
        marginRight : 10,
        fontSize : 16,
        color :"#737373",
        
    }
})
