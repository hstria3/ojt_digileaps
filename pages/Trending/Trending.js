import React,{Component} from 'react';
import {Text,View,Button,StyleSheet,ScrollView} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Header from '../../component/Header';
import NewsList from '../NewsList/NewsList'
const Trending = ({navigation}) => {
    return ( 
        <View>
            <Header title = {"Trending"} nav = {navigation}/>
            <View style = {{padding: 15, flexDirection :"row",borderBottomWidth : 1, borderBottomColor : "gray"}}>
                
                <Text style = {styles.menuBar}>Semua</Text>
                <Text style = {styles.menuBar}>Premium</Text>

            </View>
            <ScrollView>
                <NewsList nav = {navigation}/>
            </ScrollView>
        </View>
    );
    
}

const styles = StyleSheet.create({
    menuBar :{
        marginRight : 10,
        fontSize : 16,
        color :"#737373",
        
    }
})
 
 
export default Trending;