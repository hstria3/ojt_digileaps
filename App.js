import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Splash from './pages/Splash/splash'
import Router from './router/Router';
import DrawNav from './router/Drawer';
import NewList from './pages/NewsList/NewsList'

const App = () => {
  return (
    <NavigationContainer >
        <Router />
    </NavigationContainer>
    
  );
}

export default App;
