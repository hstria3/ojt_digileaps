import React,{Component} from 'react';
import {Text,View,Button,Dimensions,StyleSheet,TouchableOpacity,Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {pic1} from '../assets/';

class HeaderHome extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (
            <View style = {styles.header}>
                
                <Text style = {styles.headerText}>{this.props.title}</Text>
                <TouchableOpacity>
                    <Icon name ="search" size = {24} color = "#fff" />
                </TouchableOpacity>
                <TouchableOpacity onPress = {() => this.props.nav.openDrawer()}>
                    <Image source = {pic1} resizeMode={'cover'} style = {styles.image}  />
                </TouchableOpacity>
                
                
            </View>
          );
    }
}
 
export default HeaderHome;

const styles = StyleSheet.create({
    header :{
        width :Dimensions.get('window').width,
        height : 60,
        backgroundColor : "#5bb7e5",
        padding : 20,
        alignItems : "center",
        flexDirection : "row",
                
    },
    headerText :{
        color : "#fff",
        fontSize : 22,
        marginRight :200,
    },
    image :{
        margin : 20,
        width : 40,
        height : 40,
        borderRadius : 50
        
    }
    
})
