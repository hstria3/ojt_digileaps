import React,{Component} from 'react';
import {Text,View,Button,Dimensions,StyleSheet,TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

const Header = ({title,nav}) => {
    return (
        <View style = {styles.header}>
            
            <TouchableOpacity  >

                <Icon onPress = {()=> nav.goBack() } name = "arrow-left" size = {22} color = "#fff"/>
            </TouchableOpacity >
            <Text style = {styles.headerText}>{title}</Text>
                
        </View>
    )
}

 
const styles = StyleSheet.create({
    header :{
        width :Dimensions.get('window').width,
        height : 60,
        backgroundColor : "#5bb7e5",
        padding : 10,
        alignItems : "center",
        flexDirection : "row",
                
    },
    headerText :{
        color : "#fff",
        fontSize : 22,
        padding : 10,
    }
})


export default Header;
