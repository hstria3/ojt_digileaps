import React from 'react';
import {DrawerContentScrollView,DrawerItem} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/FontAwesome5'
import {View} from 'react-native'
import {Avatar, Title,Text} from 'react-native-paper'
import Animate from 'react-native-reanimated';
import {pic1} from '../assets/';


function Sidebar (props){

    return(
        <DrawerContentScrollView {...props}>

                <View style = {{padding:10}}>
                    <Icon name = "arrow-left" size = {22} color = "#5bb7e5" onPress = {()=> props.navigation.closeDrawer()}/>
                </View>

                <View style = {{justifyContent :"center", alignItems : "center"}}>
                    <Avatar.Image source = {pic1} size = {80} />
                    <Text style = {{color : "#737373",padding : 10,fontSize : 16, marginBottom : 20}}>Pamela</Text>
                </View>

                <View style = {{paddingHorizontal : 20}}>
                    <View>
                        <DrawerItem 
                            
                            icon = {({color,size}) =>(
                                <Icon name = "user" color = {"#5bb7e5"} size = {size} />
                            )}
                            label = "Profile"
                            
                        />
                    </View>
                    
                    <View style ={{paddingLeft : 55}}>
                    <DrawerItem 

                        label = "My Subscription"
                    />
                    </View>

                    <View style ={{paddingLeft : 55}}>
                    <DrawerItem 
                        label = "Logout"
                    />
                    </View>
                    
                </View>
        
        </DrawerContentScrollView>

    );
}

export default Sidebar